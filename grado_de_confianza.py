#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 12:57:25 2020

@author: cristina.gonzalez
"""

import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path = './3285.sav'
df = pd.read_spss(path)

trust_per_person = []


for i in range(len(df['ESCACONFIANZA'])):
    try:
        cleaned_string = df['ESCACONFIANZA'][i].replace(' Se puede confiar en la mayoría de la gente', '')
        cleaned_string = cleaned_string.replace(' Nunca se es lo bastante prudente', '')
        trust_per_person.append(float(cleaned_string))
    except:
        print(df['ESCACONFIANZA'][i])
        print("Unexpected error:", sys.exc_info()[0])
        
            
average_trust = np.average(trust_per_person)
median_trust = np.median(trust_per_person)
print(average_trust)
print(median_trust)


plt.hist(trust_per_person, bins=10, linewidth=1, edgecolor='black')
plt.grid(axis='y', alpha=0.1)
ax= plt.subplot()
ax.set_xticks([1,2,3,4,5,6,7,8,9,10])
plt.subplot
plt.xlabel('Escala del 1 al 10')
plt.ylabel('Num. personas encuestadas')
plt.title('Grado de confianza en la gente')
plt.axvline(average_trust, color='k', linestyle='dashed', linewidth=0.3)
plt.text(10.-4,200,'media: 5,7', alpha=0.4)
plt.savefig('trust_per_person.png')
plt.show()